from dataclasses import dataclass
from typing import List

import requests

class RouteType:
    BUS = 1

class ApiResource:
    pass

@dataclass
class Route(ApiResource):
    id: str
    short_name: str
    long_name: str
    type: RouteType


class Scraper:
    API_URL = "https://www.rtd-denver.com/api/nextride/"
    REFERRER = "https://www.rtd-denver.com/app/nextride"
    def make_request(self, route: str) -> ApiResource:
        response = requests.get(self.API_URL + route, headers={"Referer": self.REFERRER})
        response.raise_for_status()

        return response.json()

    def get_routes(self) -> List[Route]:
        routes = self.make_request("routes")["data"]

        for route in routes:
            match route:
                case {
                    "id": id,
                    "attributes": {
                        "routeShortName": short_name,
                        "routeLongName": long_name,
                        "routeType": type,
                    }
                }: yield Route(id, short_name, long_name, type)
                case _: raise RuntimeError("Bad JSON response")

print(list(Scraper().get_routes()))